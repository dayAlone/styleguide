const webpack = require('webpack')
const HappyPack = require('happypack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const files = '../helios/web/static/js/aviasales/'
const dir = path.join(__dirname, files)
const env = process.env.NODE_ENV === 'production' ? 'production' : 'development'

const createHappyPlugin = (id, loaders) => (
    new HappyPack({
        id,
        loaders,
        cache: true,
        verbose: false,
        threads: 4,
        tempDir: '.cache'
    })
)

const happies = [
    createHappyPlugin('coffee', ['coffee-loader?sourceMap=true']),
    createHappyPlugin('scss', [
        'style-loader',
        'css-loader?-autoprefixer&sourceMap',
        {
            loader: 'postcss-loader'
        },
        {
            loader: 'sass-loader',
            options: {
                includePaths: [path.join(__dirname, '../helios/web/static/css/')]
            }
        }
    ])
]

module.exports = {
    entry: {
        app: [
            'react-hot-loader/patch',
            'react-dev-utils/webpackHotDevClient?http://localhost:8080',
            `${__dirname}/src/index.jsx`
        ],
    },
    devtool: 'source-map',
    output: {
        filename: '[name].js',
        path: 'build',
        pathinfo: true
    },
    resolve: {
        modules: [
            'node_modules',
            path.join(__dirname, 'src'),
            path.join(__dirname, files),
            path.join(__dirname, `${files}../common/`),
            path.join(__dirname, `${files}../../css/`),
        ],
        alias: {
            'react': path.join(__dirname, 'node_modules', 'react')
        },
        plugins: [],
        extensions: ['.js', '.coffee', '.jsx', '.scss']
    },
    plugins: [
        new HtmlWebpackPlugin({
            templateContent: `
            <html>
                <head></head>
                <body>
                    <div
                        id=\'app\'
                        data-react-prop=\'${JSON.stringify(require('./src/data/params.js'))}\'
                        ></div>
                </body>
            </html>`
        }),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(env)
            }
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        ...happies,
        new webpack.LoaderOptionsPlugin({
            options: {
                plugins: happies,
                postcss: () => {
                    plugins: () =>
                        [require('autoprefixer')({
                            browsers: ['last 2 versions', 'IE >= 10', 'opera 12']
                        })]
                }
            }
        })
    ],
    module: {
        loaders: [{
            exclude: /node_modules/,
            test: /(\.js|\.jsx)$/,
            loader: 'babel-loader',
        },
        {
            test: /\.coffee?$/,
            include: [dir, path.join(__dirname, `${files}../common/`)],
            loader: 'happypack/loader?id=coffee'
        },
        {
            test: /\.(png|jpg|gif|svg)$/,
            loader: 'url-loader?limit=8192&name=build/images/[name]-[hash].[ext]'
        }, {
            test: /\.scss?$/,
            include: [
                dir,
                path.join(__dirname, `${files}../../css/`),
                path.join(__dirname, 'src')
            ],
            loader: 'happypack/loader?id=scss'
        }]
    },
}
