const webpack = require('webpack');
const path = require('path');
const files = '../helios/web/static/js/aviasales/ticket/'
const dir = path.join(__dirname, files);

console.log(dir)

module.exports = {
    title: 'Aviasales Ticket Style Guide',
    components: files + '**/*.coffee',
    webpackConfig: {
        entry: [
            'babel-polyfill'
        ],
        resolve: {
            extensions: ['.coffee']
        },
        module: {
            loaders: [
                {
                    test: /\.coffee$/,
                    include: dir,
                    loader: 'coffee-loader'
                }
            ]
        }
    }
    /*
        webpackConfig.resolve.extensions.push('.coffee')
        webpackConfig.module.loaders.push(
            ,
            {
                test: /\.scss?$/,
                include: dir,
                loaders: [
                    'css-loader?-autoprefixer&sourceMap',
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: () => [ require('autoprefixer')({ browsers: ['last 2 versions', 'IE >= 10', 'opera 12'] }) ]
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            includePaths: [dir, dir + 'css/']
                        }
                    }
                ]
            }
        );
    return webpackConfig;
    },
    */
};
