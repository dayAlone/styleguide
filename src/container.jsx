import React, { Component } from 'react'
import Form from 'form/components/standalone_form'
import resizer from 'utils/resizer'
import TripParams from 'utils/trip_params'
import 'form/form.scss'
import 'form/form_tabs.scss'
import 'aviasales-index.scss'

resizer.init()

export default class Test extends Component {
    render() {
        return (
            <div className='app'>
                <Form
                    initialParams={TripParams.getNormalizedParams()}
                    action='avia'
                />
            </div>
        )
    }

}
