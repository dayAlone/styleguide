module.exports = {
    "showHeader" : true,
    "multiway" : {
        "mainTitle": "Поиск дешевых авиабилетов",
        "callToAction": "Лучший способ купить авиабилеты дешево",
        "action": "//hydra.aviasales.ru"
    },
    "mainTitleTag" : "h1",
    "insurance" : {
        "mainTitle": "Страхование путешественников",
        "link": "https://cherehapa.ru/?partnerId=5",
        "callToAction": "Сделайте путешествие безопасным c «cherehapa.ru»"
    },
    "hotel" : {
        "mainTitle": "Поиск отелей со скидками до 60%",
        "callToAction": "Забронируйте номер по выгодной цене!"
    },
    "cars" : {
        "mainTitle": "Аренда автомобиля",
        "link": "http://www.rentalcars.com/Home.do?affiliateCode=aviasalescpc&preflang=ru&adplat=homepage&adcamp=cartab",
        "callToAction": "Закажите машину напрокат заранее"
    },
    "avia" : {
        "mainTitle": "Поиск дешевых авиабилетов",
        "insurancePopup": false,
        "callToAction": "Лучший способ купить авиабилеты дешево",
        "action": "//hydra.aviasales.ru"
    },
    "activeForm" : "avia"
}
