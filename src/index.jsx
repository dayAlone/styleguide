import React from 'react'
import { render } from 'react-dom'
import { AppContainer } from 'react-hot-loader'

import './css/index.scss'

const renderApp = () => {
    const Root = require('./container')
    render(
        <AppContainer>
            <Root />
        </AppContainer>,
        document.getElementById('app')
    )
}

renderApp()
if (module.hot) {
    module.hot.accept('./container', () => {
        renderApp()
    })
}
